%global namedreltag .Final
%global namedversion %{version}%{?namedreltag}
%global majorversion 5

Name:                hibernate-validator
Version:             5.2.4
Release:             6
Summary:             Bean Validation 1.1 (JSR 349) Reference Implementation
License:             ASL 2.0
URL:                 http://www.hibernate.org/subprojects/validator.html
Source0:             https://github.com/hibernate/hibernate-validator/archive/%{namedversion}/hibernate-validator-%{namedversion}.tar.gz
# JAXB2 and JDK7+ problems see https://hibernate.atlassian.net/browse/HV-528
Patch0:              %{name}-5.2.4.Final-jaxb.patch
Patch1:              CVE-2017-7536.patch
Patch2:              CVE-2020-10693-pre.patch
Patch3:              CVE-2020-10693-1.patch
Patch4:              CVE-2020-10693-2.patch
Patch5:              CVE-2020-10693-3.patch
Patch6:              CVE-2020-10693-4.patch
Patch7:		     CVE-2019-10219.patch

BuildRequires:       maven-local mvn(com.fasterxml:classmate) mvn(com.sun.xml.bind:jaxb-impl)
BuildRequires:       mvn(com.thoughtworks.paranamer:paranamer)
BuildRequires:       mvn(javax.annotation:javax.annotation-api) = 1.2 mvn(javax.el:javax.el-api)
BuildRequires:       mvn(javax.enterprise:cdi-api) mvn(javax.validation:validation-api)
BuildRequires:       mvn(javax.xml.bind:jaxb-api) mvn(joda-time:joda-time) mvn(junit:junit)
BuildRequires:       mvn(log4j:log4j:1.2.17) mvn(org.apache.felix:maven-bundle-plugin)
BuildRequires:       mvn(org.apache.maven.plugins:maven-enforcer-plugin)
BuildRequires:       mvn(org.codehaus.mojo:jaxb2-maven-plugin) mvn(org.glassfish.web:javax.el)
BuildRequires:       mvn(org.hibernate.javax.persistence:hibernate-jpa-2.1-api)
BuildRequires:       mvn(org.jboss.arquillian:arquillian-bom:pom:)
BuildRequires:       mvn(org.jboss.maven.plugins:maven-injection-plugin)
BuildRequires:       mvn(org.jboss.spec.javax.interceptor:jboss-interceptors-api_1.2_spec)
BuildRequires:       mvn(org.jboss.logging:jboss-logging) >= 3.1.1
BuildRequires:       mvn(org.jboss.logging:jboss-logging-processor:1)
BuildRequires:       mvn(org.jboss.maven.plugins:maven-injection-plugin)
BuildRequires:       mvn(org.jboss.shrinkwrap:shrinkwrap-bom:pom:)
BuildRequires:       mvn(org.jboss.shrinkwrap.descriptors:shrinkwrap-descriptors-bom:pom:)
BuildRequires:       mvn(org.jboss.shrinkwrap.resolver:shrinkwrap-resolver-bom:pom:)
BuildRequires:       mvn(org.jsoup:jsoup) mvn(org.testng:testng)
BuildArch:           noarch

%description
This is the reference implementation of JSR-349 - Bean Validation 1.1.
Bean Validation defines a meta-data model and API for JavaBean as well
as method validation. The default meta-data source are annotations,
with the ability to override and extend the meta-data through the
use of XML validation descriptors.

%package annotation-processor
Summary:             Hibernate Validator Annotation Processor
%description annotation-processor
Hibernate Validator Annotation Processor.

%package cdi
Summary:             Hibernate Validator Portable Extension
%description cdi
Hibernate Validator CDI Portable Extension.

%package parent
Summary:             Hibernate Validator Parent POM
%description parent
Aggregator of the Hibernate Validator modules.

%package performance
Summary:             Hibernate Validator Performance Tests
%description performance
Hibernate Validator performance tests.

%package test-utils
Summary:             Hibernate Validator Test Utils
%description test-utils
Hibernate Validator Test Utils.

%package javadoc
Summary:             Javadoc for %{name}
%description javadoc
This package contains javadoc for %{name}.

%prep
%setup -q -n %{name}-%{namedversion}
find . -name "*.jar" -delete
find . -name "validation-configuration-1.0.xsd" -delete
find . -name "validation-mapping-1.0.xsd" -delete
%patch0 -p1
%patch1 -p1
%patch2 -p1
%patch3 -p1
%patch4 -p1
%patch5 -p1
%patch6 -p1
%patch7 -p1
%pom_disable_module distribution
%pom_disable_module documentation
%pom_disable_module engine-jdk8-tests
%pom_disable_module integration
%pom_disable_module osgi
%pom_disable_module tck-runner
%pom_remove_dep :fest-assert test-utils
rm -r test-utils/src/main/java/org/hibernate/validator/testutil/ConstraintViolationAssert.java \
 test-utils/src/main/java/org/hibernate/validator/testutil/DescriptorAssert.java \
 test-utils/src/main/java/org/hibernate/validator/testutil/MessageLoggedAssertionLogger.java
%pom_remove_plugin :maven-jdocbook-plugin
%pom_remove_plugin org.zanata:zanata-maven-plugin
%pom_remove_plugin -r org.codehaus.gmaven:gmaven-plugin
%pom_remove_plugin -r org.codehaus.mojo:clirr-maven-plugin
%pom_remove_plugin -r org.codehaus.mojo:chronos-jmeter-maven-plugin
%pom_remove_plugin org.codehaus.mojo:chronos-report-maven-plugin performance
%pom_xpath_remove "pom:build/pom:extensions"
%pom_remove_plugin -r :maven-dependency-plugin
%pom_remove_plugin -r :maven-surefire-report-plugin
%pom_remove_plugin -r :animal-sniffer-maven-plugin
%pom_xpath_inject "pom:build/pom:pluginManagement/pom:plugins/pom:plugin[pom:artifactId='maven-javadoc-plugin']/pom:configuration" " <excludePackageNames>*.internal.*</excludePackageNames>"
%pom_xpath_set "pom:maven.javadoc.skip" false
%pom_xpath_inject "pom:plugin[pom:artifactId='maven-compiler-plugin']/pom:configuration" \
 "<useIncrementalCompilation>false</useIncrementalCompilation>"
%pom_xpath_set "pom:properties/pom:jboss.logging.processor.version" 1
%pom_change_dep :jboss-logging-processor ::'${jboss.logging.processor.version}' engine
rm engine/src/main/java/org/hibernate/validator/internal/engine/valuehandling/JavaFXPropertyValueUnwrapper.java

%build
%mvn_build -f -s -- -Pdist

%install
%mvn_install

%files -f .mfiles-%{name}
%doc CONTRIBUTING.md README.md changelog.txt
%license copyright.txt license.txt

%files annotation-processor -f .mfiles-%{name}-annotation-processor
%license copyright.txt license.txt

%files cdi -f .mfiles-%{name}-cdi

%files parent -f .mfiles-%{name}-parent
%license copyright.txt license.txt

%files performance -f .mfiles-%{name}-performance
%license copyright.txt license.txt

%files test-utils -f .mfiles-%{name}-test-utils
%license copyright.txt license.txt

%files javadoc -f .mfiles-javadoc
%license copyright.txt license.txt

%changelog
* Fri Jun 07 2024 Ge Wang <wang__ge@126.com> - 5.2.4-6
- Fix build failure due to jaxb2-maven-plugin updated

* Mon Nov 29 2021 lingsheng <lingsheng@huawei.com> - 5.2.4-5
- Limit javax.annotation-api version to 1.2

* Mon Aug 23 2021 houyingchao <houyingchao@huawei.com> - 5.2.4-4
- Fix CVE-2019-10219

* Mon Mar 15 2021 wangxiao <wangxiao65@huawei.com> - 5.2.4-3
- Fix CVE-2020-10693

* Sat Sep 19 2020 maminjie <maminjie1@huawei.com> - 5.2.4-2
- fix CVE-2017-7536

* Wed Aug 12 2020 maminjie <maminjie1@huawei.com> - 5.2.4-1
- package init
